# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2019-03-16
### Added
- Initial release of Hecaton

[Unreleased]: https://git.wur.nl/wijfj001/hecaton/compare/v0.1.0...master
[0.1.0]: https://git.wur.nl/wijfj001/hecaton/tags/v0.1.0

